# README #

This is a tutorial and consists of a system of web pages (initially adastra.html) and a collection of python programs. So you need a web browser and, if you want to try the programs (which I hope will be fun to do) you need python 2.7.

# Purpose

The goal of this tutorial is to give the reader some idea of the scale of the cosmos. Things are so far away that we can't usually imagine them so I propose an imaginary trip in a "magic RV" (a very small space ship that magically has all the food and water you need -- although we'll get to that eventually). Along the way I hope to communicate some of the techniques of physical simulation and, of necessity a bit of physics and some Python coding skills. 

# Audience
Anyone, presumably someone in high school or early college, who hasn't yet taken classes in basic Physics (Newtonian mechanics and some easy Einstein) and/or beginner Python. 

I think that's probably a pretty big group. This could be a huge success!

Oh, wait, AND they have to be interested. Ok, that's a much smaller group. But *I* think it's fun so I'll just knock it out and if anyone is interested, that's gravy. 



### How do I get set up? ###

There's not much to this. It's a bunch of talk-talk that explains what we're doing and some python code embedded in the web page. And I'll probably put in links to the Python programs (it's all Python 2.7. And, since it's git, you can download it in a block if you want and skip the talk-talk). 

One thing I should mention is that this is a series of simulations. The first simulation is really, really basic -- and totally, absurdly, inaccurate. They get gradually better. So if you don't want to see how they evolve (which is mostly to learn Python and Physics) you might just skip to the last simulation which I hope to be a reasonably good answer to the question "how long would it take to get to Proxima Alpha?"


### Contribution guidelines ###

This is mostly a big pile of text so most of the changes are going to be typos and maybe, if you're really ambitious, a little correction here or there. 

On the Python side I don't pretend to be the most Pythonic programmer ever but I'm really going for simple, readable code. So that's the guideline: your change can't change the answers AND has to be EASIER to read. Otherwise, it's not "better".

I think I'm going to constrain contributions to be through pull requests just so I don't get some malicious person who breaks it just because he can. 

### Who do I talk to? ###

Repo owner; nobody here but us chickens...