ROCKET NOTES
Saturn V: 34.5 e6 newtons  (7.6 e6 pounds) 
 thrust; it depends on the amount (mass) and speed of gas that the rocket fires
 escape velocity = 40,000 km/h = 4e7 m/h ~= 11000m/s
 least enough kinetic energy to escape the pull of Earth's gravity
 Rocket Engines
 RD-180: Thrust (vac.) 	4.15 MN (930,000 lbf) Thrust-to-weight ratio 	78.44  Isp (vac.) 	338 s (3.31 km/s) Burn time 	270 s
  speed of sound in air is about 340 m/s while the speed of sound in the hot gas of a rocket engine can be over 1700 m/s; 
  Expansion in the rocket nozzle then further multiplies the speed, typically between 1.5 and 2 times, giving a highly collimated hypersonic exhaust jet.
  The most important metric for the efficiency of a rocket engine is impulse per unit of propellant, this is called specific impulse (usually written I s p {\displaystyle I_{sp}} I_{sp}). 
This is either measured as a speed (the effective exhaust velocity v e {\displaystyle v_{e}} v_{e} in metres/second or ft/s) or as a time (seconds).
Rocket 							Propellants 	Isp, vacuum (s)
Space shuttle liquid engines 	LOX/LH2 	453[3]
Space shuttle solid motors 		APCP 		268[3]
Space shuttle OMS 				NTO/MMH 	313[3]
Saturn V stage 1 				LOX/RP-1 	304[3]

Below is an approximate equation for calculating the net thrust of a rocket engine:[4]

    F n = m ? v e = m ? v e - o p t + A e ( p e - p a m b ) {\displaystyle F_{n}={\dot {m}}\;v_{e}={\dot {m}}\;v_{e-opt}+A_{e}(p_{e}-p_{amb})} {\displaystyle F_{n}={\dot {m}}\;v_{e}={\dot {m}}\;v_{e-opt}+A_{e}(p_{e}-p_{amb})}

    where: 	 
    m ? {\displaystyle {\dot {m}}} {\dot {m}} 	=  exhaust gas mass flow
    v e {\displaystyle v_{e}} v_{e} 	=  effective exhaust velocity
    v e - o p t {\displaystyle v_{e-opt}} {\displaystyle v_{e-opt}} 	=  effective jet velocity when Pa = Pe
    A e {\displaystyle A_{e}} A_{e} 	=  flow area at nozzle exit plane (or the plane where the jet leaves the nozzle if separated flow)
    p e {\displaystyle p_{e}} p_{e} 	=  static pressure at nozzle exit plane
    p a m b {\displaystyle p_{amb}} p_{amb} 	=  ambient (or atmospheric) pressure
    
Fn = mv = Vo + A(Pe - Pa)
m: exhaust gas mass flow
v = effecive exhaust velocity
Vo=effective jet velocity and Pa = Pe
A = flow area at nozzle exit plane (or the plane where the jet leaves the nozzle if separated flow)
Pe = static pressure at nozzel exit plane
Pa = ambient pressrure

