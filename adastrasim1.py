#constants
SPEED_OF_LIGHT = 299792458.0 #m/s
SECONDS_PER_DAY = 86164.0
DAYS_PER_YEAR = 365.0
ONE_LIGHT_YEAR = 9.461e+15
DISTANCE_TO_GOAL_LY = 4.2
DISTANCE_TO_GOAL_METERS = 4.2 * ONE_LIGHT_YEAR
SECONDS_PER_YEAR = SECONDS_PER_DAY * DAYS_PER_YEAR
ONE_GRAVITY_ACCELERATION_IN_M_S_S =  9.80665

def display_status(Time, Position, Velocity, Destination):
    print "Time: {0} days\t Position: {1} Light Years out of {3} LY \t Vel: {2} of the Speed of Light".format(round(float(Time)/SECONDS_PER_DAY,2), round(float(Position)/ONE_LIGHT_YEAR,4), round(float(Velocity)/SPEED_OF_LIGHT,3),DISTANCE_TO_GOAL_LY)



#set up initial values
Time = 0
Position = 0.0 #m
Velocity = 0.0 #m/s
Acceleration = ONE_GRAVITY_ACCELERATION_IN_M_S_S
Destination = DISTANCE_TO_GOAL_METERS
ReportTime = (SECONDS_PER_DAY * DAYS_PER_YEAR)/4 #we'll report on our location 4 times a year

print "Initial Situation:"
display_status(Time, Position, Velocity, Destination)

# run simulation
while Position < Destination:
    Velocity += Acceleration # apply acceleration tp velocity 
    Position += Velocity # apply velocity to position
    Time += 1 #tick-tock... one more second
    if Time % ReportTime == 0: # wait for the right time to report
        display_status(Time, Position, Velocity, Destination)
print "Final situation:  "
display_status(Time, Position, Velocity, Destination)
