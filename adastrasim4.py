''' Simulation to demonstrate math for engines and fuel and problems inherent '''
#constants
SPEED_OF_LIGHT = 299792458.0 #m/s
SECONDS_PER_DAY = 86164.0
DAYS_PER_YEAR = 365.0
ONE_LIGHT_YEAR = 9.461e+15
DISTANCE_TO_GOAL_LY = 4.2
DISTANCE_TO_GOAL_METERS = 4.2 * ONE_LIGHT_YEAR
SECONDS_PER_YEAR = SECONDS_PER_DAY * DAYS_PER_YEAR
ONE_GRAVITY_ACCELERATION_IN_M_S_S =  9.80665
MEGATONNE = 1000000000 
F1_FULL_BURN_MASS = 2578 #kg/s
F1_FULL_BURN_THRUST = 6.7e+6 #Newtons
F1_MAGIC_ENGINE_EFFICIENCY = 50000 #note,if you change this to one you will simulate going to space on an F1
MAGIC_RV_MASS = 38000 #kg
STARTING_FUEL_MASS = 8.1299808e10/2 #kg
MY_THRUST = F1_MAGIC_ENGINE_EFFICIENCY * F1_FULL_BURN_THRUST
MY_BURN_MASS = F1_FULL_BURN_MASS




def mps_to_ls(Velocity):
    return float(Velocity)/float(SPEED_OF_LIGHT)

def m_to_ly(Position):
    return float(Position)/ONE_LIGHT_YEAR

def s_to_day(Time):
    return float(Time)/SECONDS_PER_DAY

def display_status(Time, Position, Velocity, Destination,FuelMass,Engines_On,Acceleration):
    if Engines_On:
        Engine_Status = "On"
    else:
        Engine_Status = "Off"
    print "Time: {0} days\t Position: {1} LY of {2} LY \t Vel: {3} SoL\tFuel Left:{4} MT\tEngine Status: {5}\tAcceleration: {6} G".format(round(s_to_day(Time),1), round(m_to_ly(Position),2), DISTANCE_TO_GOAL_LY, round( mps_to_ls(Velocity),3), round(FuelMass/MEGATONNE,0),Engine_Status,round(Acceleration/ONE_GRAVITY_ACCELERATION_IN_M_S_S,2))


#set up initial values
Time = 0
Position = 0.0 #m
Velocity = 0.0 #m/s
Destination = DISTANCE_TO_GOAL_METERS
ReportTime = int((SECONDS_PER_DAY * DAYS_PER_YEAR)/12)
Mass = MAGIC_RV_MASS + STARTING_FUEL_MASS
FuelMass = STARTING_FUEL_MASS


Turn_Around_Point = Destination + 1000 
Attained_Light_Speed = False
Reached_Turn_Around = False
Forward = 1 #directs acceleration in positive direction at first; in negative direction after turn around
Engines_On = True
Reported_Half_Fuel = False
Reported_SOL = False
Acceleration = 0

print "Days of Fuel:",STARTING_FUEL_MASS/MY_BURN_MASS/60/60/24

print "Initial Situation:"
display_status(Time, Position, Velocity, Destination,FuelMass,False,Acceleration)
# run simulation
while Position < Destination and Velocity >= 0:
    if Engines_On:
        Acceleration = Forward *  MY_THRUST / Mass
        if abs(Acceleration) > ONE_GRAVITY_ACCELERATION_IN_M_S_S:
            this_burn_mass = abs(float(MY_BURN_MASS) * (float(ONE_GRAVITY_ACCELERATION_IN_M_S_S)/float(abs(Acceleration))))
            Acceleration = ONE_GRAVITY_ACCELERATION_IN_M_S_S
        else:
            this_burn_mass = MY_BURN_MASS
        Mass += -this_burn_mass
        FuelMass += -this_burn_mass
    else:
        Acceleration = 0
    if FuelMass <= STARTING_FUEL_MASS/2 and not Reached_Turn_Around: #if we don't turn our engines off, we'll run out of fuel
        Engines_On = False
        Turn_Around_Point = Destination - Position #calculate where we need to be to turn around\
        if not Reported_Half_Fuel:
            print "\nHalf way through fuel; turning engines off.\n"
            display_status(Time,Position, Velocity, Destination,FuelMass,Engines_On,0)
            Reported_Half_Fuel = True
    if Position >= Turn_Around_Point and not Reached_Turn_Around:
        Reached_Turn_Around = True
        Forward = -1
        Engines_On = True
        print "\nWe reached Turn Around: decelerating!"
        display_status(Time,Position, Velocity, Destination,FuelMass,Engines_On,Acceleration)
    Velocity += Acceleration # apply acceleration tp velocity 
    if Velocity >= SPEED_OF_LIGHT: # Are we over the speed limit of the speed of light?
        Velocity = SPEED_OF_LIGHT # Impose the speed limit
        Engines_On = False # turn the engines off until turn around point
        if not Reported_SOL: 
            Reported_SOL = True
            print "Reached Speed of Light; turning engines off"
            display_status(Time, Position, Velocity,  Destination,FuelMass,Engines_On,Acceleration)
            
    if Velocity <=0.0: #stop simulation when we stop
        print "\nWe've Stopped!"
            
    Position += Velocity # apply velocity to position
    Time += 1 #tick-tock... one more second
    if Time % ReportTime == 0:  # wait for the right time to report
        display_status(Time, Position, Velocity, Destination,FuelMass,Engines_On,Acceleration)

print "Final situation:  "
display_status(Time, Position, Velocity, Destination,FuelMass,Engines_On,Acceleration)

