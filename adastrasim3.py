#constants
SPEED_OF_LIGHT = 299792458.0 #m/s
SECONDS_PER_DAY = 86164.0
DAYS_PER_YEAR = 365.0
ONE_LIGHT_YEAR = 9.461e+15
DISTANCE_TO_GOAL_LY = 4.2
DISTANCE_TO_GOAL_METERS = 4.2 * ONE_LIGHT_YEAR
SECONDS_PER_YEAR = SECONDS_PER_DAY * DAYS_PER_YEAR
ONE_GRAVITY_ACCELERATION_IN_M_S_S =  9.80665

def mps_to_ls(Velocity):
    return float(Velocity)/float(SPEED_OF_LIGHT)

def m_to_ly(Position):
    return float(Position)/ONE_LIGHT_YEAR

def display_status(Time, Position, Velocity, Destination):
    print "Time: {0} days\t Position: {1} Light Years out of {3} LY \t Vel: {2} of the Speed of Light".format(round(float(Time)/SECONDS_PER_DAY,2), round(m_to_ly(Position),4), round( mps_to_ls(Velocity),3),DISTANCE_TO_GOAL_LY)


#set up initial values
Time = 0
Position = 0.0 #m
Velocity = 0.0 #m/s
Acceleration = 9.8 #1 G in meters/s/s
Destination = DISTANCE_TO_GOAL_METERS
ReportTime = (SECONDS_PER_DAY * DAYS_PER_YEAR)/4 #we'll report on our location 4 times a year

Turn_Around_Point = Destination + 1000 
Attained_Light_Speed = False
Reached_Turn_Around = False

print "Initial Situation:"
display_status(Time, Position, Velocity, Destination)
# run simulation
while Position < Destination and Velocity >= 0:
    if Position >= Turn_Around_Point and not Reached_Turn_Around:
        Reached_Turn_Around = True
        Acceleration = -Acceleration 
        print "We reached Turn Around: decelerating!"
        display_status(Time,Position, Velocity, Destination)
        print "Time (days): {0}; Pos: {1}LY;  Vel: {2} LS".format(round(Time/SECONDS_PER_DAY,2), round(m_to_ly(Position),3), round(mps_to_ls(Velocity),3))
    Velocity += Acceleration # apply acceleration tp velocity 

    if Velocity >= SPEED_OF_LIGHT: # Are we over the speed limit of the speed of light?
        Velocity = SPEED_OF_LIGHT # Impose the speed limit
        if not Attained_Light_Speed: 
            Attained_Light_Speed = True
            Turn_Around_Point = Destination - Position #calculate where we need to be to turn around
            print "\nWe attained light speed! Calculating turn around."
            print "Time (days): {0}; Pos: {1} (m); Vel: {2}(m/s); Vel: {3} fraction of speed of light".format(round(Time/SECONDS_PER_DAY,2), Position, round(Velocity), round(float(Velocity)/SPEED_OF_LIGHT,3))
            print "Turn around will happen at {0} (m). \n".format (Turn_Around_Point)
    if Velocity <=0.0: #stop simulation when we stop
        print "\nWe've Stopped!"
        display_status(Time, Position, Velocity, Destination)
            
    Position += Velocity # apply velocity to position
    Time += 1 #tick-tock... one more second
    if Time % ReportTime == 0:  # wait for the right time to report
        display_status(Time, Position, Velocity, Destination)
        #print "Time (days): {0}; Pos: {1}(m); Pos: {4}(LY); Vel: {2}(m/s); Vel: {3} fraction of speed of light".format(round(Time/SECONDS_PER_DAY,2), Position, round(Velocity), round(float(Velocity)/SPEED_OF_LIGHT,3),round(Position/ONE_LIGHT_YEAR,2))
        #print "Time (days): {0}; Pos: {1} (m); Vel: {2}(m/s); Vel: {3} fraction of speed of light".format(round(Time/SECONDS_PER_DAY,2), Position, round(Velocity), round(float(Velocity)/SPEED_OF_LIGHT,3))

print "Final situation:  "
#Time (days): {0}; Pos: {1}; Vel: {2}(m/s); Vel: {3} fraction of speed of light".format(round(Time/SECONDS_PER_DAY,2), Position, round(Velocity), round(float(Velocity)/SPEED_OF_LIGHT,3))
display_status(Time, Position, Velocity, Destination)

